create database if not exists StudentDB
character set utf8 ;
use StudentDB;

CREATE TABLE IF NOT EXISTS speciality (
	id INT NOT NULL AUTO_INCREMENT, 
	name VARCHAR(45) NOT NULL,
	CONSTRAINT PK_speciality_id
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS university_group (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(10) NOT NULL,
	code INT(3) NOT NULL,
	entry_year INT(4) NULL,
	speciality_id INT not null,
	CONSTRAINT PK_university_group_id_speciality_id
	PRIMARY KEY (id, speciality_id),
	INDEX fk_university_group_speciality_idx (speciality_id ASC),
	CONSTRAINT fk_university_group_speciality
		FOREIGN KEY (speciality_id)
		REFERENCES speciality (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS student (
	id int NOT NULL auto_increment, 
	first_name VARCHAR (20) NOT NULL ,
    second_name VARCHAR (20) NOT NULL ,
    middle_name VARCHAR (20) NOT NULL ,
    photo BLOB NULL,
	biography VARCHAR(300) NOT NULL ,
	birth_year INT(4) NULL,
	address VARCHAR(30) NULL,
	raiting DOUBLE NULL,
	scholarship INT(4) NULL,
    group_id INT NOT NULL,
    CONSTRAINT PK_student_id_university_group_id
	PRIMARY KEY (id, group_id),
	INDEX fk_student_university_group_idx (group_id ASC),
	CONSTRAINT fk_student_university_group
		FOREIGN KEY (group_id)
		REFERENCES university_group (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS lecturer (
  id INT NOT NULL AUTO_INCREMENT,
  name_surname_middle_name VARCHAR(45) NULL,
	CONSTRAINT PK_lecturer_id
	PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS subject (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  module_one INT(2) NULL,
  module_two INT(2) NULL,
  type VARCHAR(10) NULL,
  of_100 INT(3) NULL,
  of_5 INT(1) NULL,
  semester_number INT(1) NULL,
  lecturer_id INT NOT NULL,
  CONSTRAINT PK_subject_id_lecturer_id
	PRIMARY KEY (id, lecturer_id),
	INDEX fk_subject_lecturer_idx (lecturer_id ASC),
	CONSTRAINT fk_subject_lecturer
		FOREIGN KEY (lecturer_id)
		REFERENCES lecturer (id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);

CREATE TABLE student_has_subject (
  student_id int NOT NULL,
  student_group_id int NOT NULL,
  subject_id int NOT NULL,

  CONSTRAINT PK_student_has_subject_student_id_student_group_id_subject_id
  PRIMARY KEY (student_id, student_group_id, subject_id),
  
  INDEX FK_student_has_subject_subject_idx (subject_id ASC),
  INDEX FK_student_has_subject_student_idx (student_id ASC, student_group_id ASC),
  
  CONSTRAINT FK_student_has_subject_student
    FOREIGN KEY (student_id, student_group_id)
    REFERENCES student (id, group_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT FK_student_has_subject_subject
    FOREIGN KEY (subject_id)
    REFERENCES subject (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
    

   insert into lecturer (name_surname_middle_name)
	values 	('Petrenko Petro Ivanovych'),
			('Kosulo Ivan Ivanovych'),
			('Gayda Petro Olegovych');
            
            insert into speciality (name)
    values	('Computer Science'),
			('Computer Engineering'),
			('Programming Engineering');
    
    insert into university_group (name, code, entry_year, speciality_id)
    values 	('KN-22', 012, 2018, 1),
			('PI-11', 033, 2019, 2),
			('KI-31', 015, 2018, 1);
    
    insert into subject (name, module_one, module_two , type, of_100, of_5, semester_number, lecturer_id)
    values 	('Math', 45, 30, 'exam', 75, 4, 1, 1),
			('English', 30, 30, 'exam', 60, 3, 2, 2),
			('History', 20, 25, 'zalik', 45, 2, 2, 3);
    
    insert into student (first_name, second_name, middle_name, biography, birth_year, address, raiting, scholarship, group_id)
    values 	('Andrii', 'Vytr', 'Rostyslavovych', 'Goodboy', 1994, 'Lviv', 750.1, 700, 1),
			('Oleg', 'Pok', 'Vadymovych', 'Badboy', 1998, 'Odessa', 550.1, 900, 2),
			('Sergii', 'Kaplunov', 'Mykolovych', 'Wonderfull!', 1999, 'Kyiv', 950.1, 700, 3);

